%% Q3 2014

%% a)

s --> zeroPart(Count0), onePart(Count1), twoPart(Count2), {Count2 =:= Count1 + Count0}.

zeroPart(NewCount) --> [0], zeroPart(Count), {NewCount is Count + 1}.
zeroPart(0) --> [].

onePart(NewCount) --> [1], onePart(Count), {NewCount is Count + 1}.
onePart(0) --> [].

twoPart(NewCount) --> [2], twoPart(Count), {NewCount is Count + 1}.
twoPart(0) --> [].

%% b)

s1 --> zeroPart.

zeroPart --> [].
zeroPart --> [0], zeroPartList, [2].

zeroPartList --> onePart.
zeroPartList --> [0], zeroPartList, [2].


onePart --> [1], onePartList, [2].

onePartList --> [].
onePartList --> [1], onePartList, [2].

%% c)

%% A difference list is a list used to specify the elements that are different in a pair of lists. In prolog they are useful as this allows us to construct context free grammars to verify strings according to that grammar. In fact the Definite Clause Grammar (DCG) implementation uses difference lists to do this.

%% d)

s2(X, X).
s2(X, Y) :- zeroPart2(X, X2), s2(X2, X3), twoPart2(X3, Y).

zeroPart2([0|X], X).

twoPart2([2|X], X).

%% e) wwwrev

s3 -->  w([], X), w2(X), wRev(X).

w(_, []) --> [].
w(X, Z) --> [0], w(X, Y), {append([0], Y, Z)}.
w(X, Z) --> [1], w(X, Y), {append([1], Y, Z)}.

w2([]) --> [].
w2([H|T]) --> [H], w2(T).

wRev([]) --> [].
wRev([H|T]) --> wRev(T), [H].


