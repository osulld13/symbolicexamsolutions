
%% 2013 Solutions

%% Q3

%% a)

reverse1([], []).
reverse1([H|T], Reversed) :- reverse1(T, X), append(X, [H], Reversed).