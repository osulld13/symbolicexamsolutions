%Exam Solutions for the 2014 Paper

%Q1

% a) listSum is not tail recursive as the recursive elements of the function are called before the computation is complete thus the answer to the problem is calculated on way up the derivation tree. In order for the predicate to be tail recursive the answer needs to have been calculated by the time the lowest level of recursion terminates.

%b)

%% listSum([], 0).
%% listSum([Head|Tail]) :0 listSum(Tail, TailSum), Sum is Head + TailSum.

listSum(X, Y) :- tRListSum(X, 0, Y).

tRListSum([], Y, Y).
tRListSum([H|T], Y, Z) :- NewY is Y + H, tRListSum(T, NewY, Z).

%c)

%tail recursive, listProd(list, prod)

listProd(X, Y) :- tRListProd(X, 1, Y).

tRListProd([], Y, Y).
tRListProd([H|T], Y, Z) :- NewY is Y * H, tRListProd(T, NewY, Z). 

%d) list2N(Int, List)

list2N(In, Out) :- list2NHelper(In, [], Out). 

list2NHelper(0, Y, Y).
list2NHelper(X, Y, Z) :- X > 0, NewX is X - 1, append(Y, [X], NewL), list2NHelper(NewX, NewL, Z).

%e) 

% nonInc, liist is a non increasing finite list of numbers

nonInc([_|[]]).
nonInc([H1, H2|T]) :- H1 > H2, nonInc([H2|T]).

%f)

%sumList, given non negative integer Sum returns a non increasing list of integers that add up to sum

sumList(X, OutList) :- list2N(X, XList), sumListHelper(X, XList, [], OutList).

sumListHelper(X, _, InList, InList) :- listSum(InList, Y), X == Y.
sumListHelper(X, XList, InList, OutList) :- member(Y, XList), append(InList, [Y], NewInList), listSum(NewInList, Z), Z =< X, sumListHelper(X, XList, NewInList, OutList).
